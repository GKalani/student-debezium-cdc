package com.kgk.studentdebeziumcdc;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class StudentDebeziumCdcApplicationTests {

	@Test
	void contextLoads() {

	}

	@Test
	void test(){
		assertEquals(42, Integer.sum(19, 23));
	}

}
