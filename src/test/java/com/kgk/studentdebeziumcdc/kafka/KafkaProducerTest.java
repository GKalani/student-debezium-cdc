package com.kgk.studentdebeziumcdc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kgk.studentdebeziumcdc.dto.ShipmentCustomer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class KafkaProducerTest {

    @Autowired
    KafkaProducer kafkaProducer;

    @Test
    void testKafkaProducerTest(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonKafkaRecord = new HashMap<>();
        Map<String, Object> jsonMetadata = new HashMap<>();
        Map<String, Object> jsonBodyMap = new HashMap<>();

        String topicName = "TEST"; //QA_TA_FTP_SCHEDULE_KEY
        int loopStart = 1;
        int i = loopStart;
        int loopEnd = loopStart + 0;
        for (;i < loopEnd; i++) {
            Map<String, Object> message = Map.of("tripPlanNumber", i);
            jsonBodyMap.put("operation", "CREATE");
            jsonBodyMap.put("data", message);
            jsonBodyMap.put("table", "ftp_schedule");

            jsonMetadata.put("src", "DBNG");
            jsonMetadata.put("createdTime", new Date().getTime());
            jsonMetadata.put("trackingId", "a6068be6-f212-4ee2-8b90-8db3b8c31942");

            jsonKafkaRecord.put("body", jsonBodyMap);
            jsonKafkaRecord.put("metadata", jsonMetadata);

            try {
                String json1 = objectMapper.writeValueAsString(jsonKafkaRecord);
                kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json1);

                jsonBodyMap.replace("operation", "UPDATE");
                String json2 = objectMapper.writeValueAsString(jsonKafkaRecord);
                kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json2);

                if(i%3 == 0) {
                    message = Map.of("tripPlanNumber", i - 1);
                    jsonBodyMap.replace("operation", "DELETE");
                    jsonBodyMap.replace("data", message);
                    String json3 = objectMapper.writeValueAsString(jsonKafkaRecord);
                    kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json3);
                }

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    void testAll(){
        testKafkaProducerForCurrentShipment();
        testKafkaProducerForHistoryShipment();
        testKafkaProducerForCurrentShipmentCustomer();
    }
    @Test
    void testKafkaProducerForCurrentShipment(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonKafkaRecord = new HashMap<>();
        Map<String, Object> jsonMetadata = new HashMap<>();
        Map<String, Object> jsonBodyMap = new HashMap<>();

        String topicName = "QA_TA_FTP_SCHEDULE_KEY"; //QA_TA_FTP_SCHEDULE_KEY

        List<Long> tpnList = Arrays.asList(494682031L, 45050858L, 49579762L, 49909359L, 49912930L, 49912938L,
                49913104L, 49913105L, 49913114L, 49913117L, 49913118L, 45877908L, 47801421L, 48881454L, 49116648L, 49425187L,
                494682032L, 49773667L, 49795306L, 49823147L, 49823148L, 49823149L, 49823151L, 49823152L, 49823153L, 49849594L, 49849621L,
                49860268L, 49860272L, 49860273L, 49860281L, 49860282L, 49860285L, 49860286L, 49860287L, 49860288L, 49860289L,
                494682033L, 49860301L, 49860302L, 49860303L, 49860304L, 49860305L, 49860306L, 49860349L, 49860350L, 49860351L, 49860354L,
                49912939L, 49913916L, 49913919L, 49913943L, 49913949L, 49913966L, 49914602L, 49920143L, 49922195L, 49922543L,
                494682034L, 49923546L, 49930924L, 49934749L, 49939231L, 49939233L, 49939234L, 49940174L, 49940354L, 49940355L, 49941068L,
                49941069L, 49941071L, 49941081L, 49941085L, 49941086L, 49941088L, 49941248L, 49944859L, 49948244L, 49948249L,
                494682035L, 49948250L, 49950530L, 49950531L, 49950532L, 49950534L, 49950537L, 49950538L, 49950539L, 49950540L, 49950544L,
                49950546L, 49950547L, 49950548L, 49950549L, 49950550L, 49950554L, 49950556L, 49950557L, 49950558L, 49950560L,
                49950561L, 49950563L, 49950564L, 49950565L, 49950566L, 49953200L, 49953209L, 49958518L, 49958519L, 49958521L,
                49958522L, 49958523L, 49958524L, 49958525L, 49958526L, 49958527L, 49958528L, 49958530L, 49958531L, 49958532L,
                49958534L, 49958535L, 49958536L, 49958538L, 49958539L, 49958540L, 49960801L, 49960803L, 49960808L, 49960810L);

        List<Long> deletedTpns = Arrays.asList(494682031L, 494682032L, 494682033L, 494682034L, 494682035L);


        int loopStart = 0;
        int i = loopStart;
        int loopEnd = tpnList.size()                ;
        //for (;i < loopEnd; i++) {

        tpnList.forEach(tpn -> {

            Map<String, Object> message = Map.of("tripPlanNumber", tpn);
            jsonBodyMap.put("operation", "CREATE");
            jsonBodyMap.put("data", message);
            jsonBodyMap.put("table", "ftp_schedule");

            jsonMetadata.put("src", "DBNG");
            jsonMetadata.put("createdTime", new Date().getTime());
            jsonMetadata.put("trackingId", "a6068be6-f212-4ee2-8b90-8db3b8c31942");

            jsonKafkaRecord.put("body", jsonBodyMap);
            jsonKafkaRecord.put("metadata", jsonMetadata);

            try {
                //String json1 = objectMapper.writeValueAsString(jsonKafkaRecord);
                //kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json1);

                if (!deletedTpns.contains(tpn)) {
                    jsonBodyMap.replace("operation", "UPDATE");
                    String json2 = objectMapper.writeValueAsString(jsonKafkaRecord);
                    kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json2);
                }else{
                    message = Map.of("tripPlanNumber", tpn);
                    jsonBodyMap.replace("operation", "DELETE");
                    jsonBodyMap.replace("data", message);
                    String json3 = objectMapper.writeValueAsString(jsonKafkaRecord);
                    kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json3);
                }

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    void testKafkaProducerForHistoryShipment(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonKafkaRecord = new HashMap<>();
        Map<String, Object> jsonMetadata = new HashMap<>();
        Map<String, Object> jsonBodyMap = new HashMap<>();

        String topicName = "QA_TA_TRIP_PERFORMANCE_KEY"; //QA_TA_FTP_SCHEDULE_KEY

        List<Long> tpnList = Arrays.asList(48121238L,48806961L,49172417L,49184945L,49909357L,49909359L,
                49912928L,49912929L,494682031L, 49912930L,49912931L,494682032L, 49912937L,49912938L,49913104L,49913105L,49913106L,
                49913108L,49913109L,494682033L, 49913110L,49913111L,49913113L, 494682034L, 49913114L,49913115L,49913116L,
                49913117L,49913118L,49913920L, 494682035L);

        List<Long> deletedTpns = Arrays.asList(494682031L, 494682032L, 494682033L, 494682034L, 494682035L);


        int loopStart = 0;
        int i = loopStart;
        int loopEnd = tpnList.size()                ;
        //for (;i < loopEnd; i++) {

        tpnList.forEach(tpn -> {

            Map<String, Object> message = Map.of("tripPlanNumber", tpn);
            jsonBodyMap.put("operation", "CREATE");
            jsonBodyMap.put("data", message);
            jsonBodyMap.put("table", "trip_performance");

            jsonMetadata.put("src", "DBNG");
            jsonMetadata.put("createdTime", new Date().getTime());
            jsonMetadata.put("trackingId", "a6068be6-f212-4ee2-8b90-8db3b8c31942");

            jsonKafkaRecord.put("body", jsonBodyMap);
            jsonKafkaRecord.put("metadata", jsonMetadata);

            try {
                //String json1 = objectMapper.writeValueAsString(jsonKafkaRecord);
                //kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json1);

                if (!deletedTpns.contains(tpn)) {
                    jsonBodyMap.replace("operation", "UPDATE");
                    String json2 = objectMapper.writeValueAsString(jsonKafkaRecord);
                    kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json2);
                }else{
                    message = Map.of("tripPlanNumber", tpn);
                    jsonBodyMap.replace("operation", "DELETE");
                    jsonBodyMap.replace("data", message);
                    String json3 = objectMapper.writeValueAsString(jsonKafkaRecord);
                    kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json3);
                }

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    void testKafkaProducerForCurrentShipmentCustomer(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonKafkaRecord = new HashMap<>();
        Map<String, Object> jsonMetadata = new HashMap<>();
        Map<String, Object> jsonBodyMap = new HashMap<>();

        String topicName = "QA_TA_FTP_SCHEDULE_CUSTOMER_KEY";
        String tableName = "ftp_schedule_customer";

        List<ShipmentCustomer> currentShipmentCustomersList = List.of(
                ShipmentCustomer.builder().tripPlanNumber(40312742L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(40312742L).orgPoolId(2L).build(),
                ShipmentCustomer.builder().tripPlanNumber(40312742L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(499166111L).orgPoolId(157980L).build(),
                ShipmentCustomer.builder().tripPlanNumber(43240513L).orgPoolId(100L).build(),
                ShipmentCustomer.builder().tripPlanNumber(48872391L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(403127421L).orgPoolId(2L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49916611L).orgPoolId(157980L).build(),
                ShipmentCustomer.builder().tripPlanNumber(41770033L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(417700331L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49582537L).orgPoolId(157980L).build(),
                ShipmentCustomer.builder().tripPlanNumber(495825371L).orgPoolId(157980L).build()
        );

     /*   List<Long> tpnList = Arrays.asList(48121238L,48806961L,49172417L,49184945L,49909357L,49909359L,
                49912928L,49912929L,494682031L, 49912930L,49912931L,494682032L, 49912937L,49912938L,49913104L,49913105L,49913106L,
                49913108L,49913109L,494682033L, 49913110L,49913111L,49913113L, 494682034L, 49913114L,49913115L,49913116L,
                49913117L,49913118L,49913920L, 494682035L);
*/
        List<ShipmentCustomer> deletedCurrentShipmentCustomersList
                = List.of(
                currentShipmentCustomersList.get(0),
                currentShipmentCustomersList.get(3),
                currentShipmentCustomersList.get(6),
                currentShipmentCustomersList.get(9),
                currentShipmentCustomersList.get(11)
        );


        int loopStart = 0;
        int i = loopStart;
        int loopEnd = currentShipmentCustomersList.size()                ;
        //for (;i < loopEnd; i++) {

        currentShipmentCustomersList.forEach(cdtCusrrent -> {
            String key = cdtCusrrent.getTripPlanNumber() + "-" + cdtCusrrent.getOrgPoolId();
            String kafkaRecord;


            Map<String, Object> message = Map.of("tripPlanNumber", cdtCusrrent.getTripPlanNumber(),
                    "organizationPoolId", cdtCusrrent.getOrgPoolId());
            jsonBodyMap.put("operation", "INSERT");
            jsonBodyMap.put("data", message);
            jsonBodyMap.put("table", tableName);

            jsonMetadata.put("src", "DBNG");
            jsonMetadata.put("createdTime", new Date().getTime());
            jsonMetadata.put("trackingId", "a6068be6-f212-4ee2-8b90-8db3b8c31942");

            jsonKafkaRecord.put("body", jsonBodyMap);
            jsonKafkaRecord.put("metadata", jsonMetadata);

            try {
                //String json1 = objectMapper.writeValueAsString(jsonKafkaRecord);
                //kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json1);

                if (!deletedCurrentShipmentCustomersList.contains(cdtCusrrent)) {
                    jsonBodyMap.replace("operation", "UPDATE");
                    kafkaRecord = objectMapper.writeValueAsString(jsonKafkaRecord);

                }else{
                    jsonBodyMap.replace("operation", "DELETE");
                    kafkaRecord = objectMapper.writeValueAsString(jsonKafkaRecord);

                }
                kafkaProducer.sendMessage(topicName, key, kafkaRecord);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }
    @Test
    void testKafkaProducerForHistoryShipmentCustomer(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonKafkaRecord = new HashMap<>();

        String topicName = "QA_TA_HIS_FTP_SCHEDULE_CUSTOMER_KEY";
        String tableName = "his_ftp_schedule_customer";

        List<ShipmentCustomer> recordList = List.of(
                ShipmentCustomer.builder().tripPlanNumber(403127421L).orgPoolId(2L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49859097L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49863650L).orgPoolId(8715L).build(),
                ShipmentCustomer.builder().tripPlanNumber(417700331L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49920290L).orgPoolId(157980L).build(),
                ShipmentCustomer.builder().tripPlanNumber(43588583L).orgPoolId(157980L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49605766L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(45367893L).orgPoolId(157980L).build(),
                ShipmentCustomer.builder().tripPlanNumber(453678935L).orgPoolId(157980L).build()
        );

        List<ShipmentCustomer> deletedRecordsList
                = List.of(
                recordList.get(0),
                recordList.get(3),
                recordList.get(8)
        );


        recordList.forEach(cdtCusrrent -> {
            String key = cdtCusrrent.getTripPlanNumber() + "-" + cdtCusrrent.getOrgPoolId();
            String kafkaRecord;

            Map<String, Object> message = Map.of("tripPlanNumber", cdtCusrrent.getTripPlanNumber(),
                    "organizationPoolId", cdtCusrrent.getOrgPoolId());
            Map<String, Object> jsonBodyMap = getBodyJson(message, tableName);

            jsonKafkaRecord.put("body", jsonBodyMap);
            jsonKafkaRecord.put("metadata", getJsonMetadata());

            try {
                //String json1 = objectMapper.writeValueAsString(jsonKafkaRecord);
                //kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json1);

                if (!deletedRecordsList.contains(cdtCusrrent)) {
                    jsonBodyMap.replace("operation", "UPDATE");
                    kafkaRecord = objectMapper.writeValueAsString(jsonKafkaRecord);

                }else{
                    jsonBodyMap.replace("operation", "DELETE");
                    kafkaRecord = objectMapper.writeValueAsString(jsonKafkaRecord);

                }
                kafkaProducer.sendMessage(topicName, key, kafkaRecord);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    void testKafkaProducerForTripDetailOneCustomer(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonKafkaRecord = new HashMap<>();

        String topicName = "QA_TA_TRIP_DETAIL1_CUSTOMER_KEY";
        String tableName = "trip_detail1_customer";

        List<ShipmentCustomer> recordList = List.of(
                ShipmentCustomer.builder().tripPlanNumber(49854657L).orgPoolId(2L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49854657L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(50010200L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49869730L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(498697301L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49868318L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49854262L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49927886L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(453678935L).orgPoolId(123L).build()
        );

        List<ShipmentCustomer> deletedRecordsList
                = List.of(
                recordList.get(0),
                recordList.get(4),
                recordList.get(8)
        );


        recordList.forEach(cdtCusrrent -> {
            String key = cdtCusrrent.getTripPlanNumber() + "-" + cdtCusrrent.getOrgPoolId();
            String kafkaRecord;

            Map<String, Object> message = Map.of("tripPlanNumber", cdtCusrrent.getTripPlanNumber(),
                    "organizationPoolId", cdtCusrrent.getOrgPoolId());
            Map<String, Object> jsonBodyMap = getBodyJson(message, tableName);

            jsonKafkaRecord.put("body", jsonBodyMap);
            jsonKafkaRecord.put("metadata", getJsonMetadata());

            try {
                //String json1 = objectMapper.writeValueAsString(jsonKafkaRecord);
                //kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json1);

                if (!deletedRecordsList.contains(cdtCusrrent)) {
                    jsonBodyMap.replace("operation", "UPDATE");
                    kafkaRecord = objectMapper.writeValueAsString(jsonKafkaRecord);

                }else{
                    jsonBodyMap.replace("operation", "DELETE");
                    kafkaRecord = objectMapper.writeValueAsString(jsonKafkaRecord);

                }
                kafkaProducer.sendMessage(topicName, key, kafkaRecord);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }
     @Test
    void testKafkaProducerForHisTripDetailOneCustomer(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonKafkaRecord = new HashMap<>();

        String topicName = "QA_TA_HIS_TRIP_DETAIL1_CUSTOMER_KEY";
        String tableName = "his_trip_detail1_customer";

        List<ShipmentCustomer> recordList = List.of(
                ShipmentCustomer.builder().tripPlanNumber(49854657L).orgPoolId(2L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49856981L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49856980L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49996758L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(498697301L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49928013L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49868439L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(49934278L).orgPoolId(1L).build(),
                ShipmentCustomer.builder().tripPlanNumber(453678935L).orgPoolId(123L).build()
        );

        List<ShipmentCustomer> deletedRecordsList
                = List.of(
                recordList.get(0),
                recordList.get(4),
                recordList.get(8)
        );


        recordList.forEach(cdtCusrrent -> {
            String key = cdtCusrrent.getTripPlanNumber() + "-" + cdtCusrrent.getOrgPoolId();
            String kafkaRecord;

            Map<String, Object> message = Map.of("tripPlanNumber", cdtCusrrent.getTripPlanNumber(),
                    "organizationPoolId", cdtCusrrent.getOrgPoolId());
            Map<String, Object> jsonBodyMap = getBodyJson(message, tableName);

            jsonKafkaRecord.put("body", jsonBodyMap);
            jsonKafkaRecord.put("metadata", getJsonMetadata());

            try {
                //String json1 = objectMapper.writeValueAsString(jsonKafkaRecord);
                //kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json1);

                if (!deletedRecordsList.contains(cdtCusrrent)) {
                    jsonBodyMap.replace("operation", "UPDATE");
                    kafkaRecord = objectMapper.writeValueAsString(jsonKafkaRecord);

                }else{
                    jsonBodyMap.replace("operation", "DELETE");
                    kafkaRecord = objectMapper.writeValueAsString(jsonKafkaRecord);

                }
                kafkaProducer.sendMessage(topicName, key, kafkaRecord);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    void testKafkaProducerForTripDetails(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonKafkaRecord = new HashMap<>();
        Map<String, Object> jsonMetadata = new HashMap<>();
        Map<String, Object> jsonBodyMap = new HashMap<>();

        String topicName = "QA_TA_TRIP_DETAIL1_KEY"; //QA_TA_FTP_SCHEDULE_KEY

        List<Long> recordList = Arrays.asList(494682031L, 49495800L,49456717L, 494682032L, 47213964L, 46114751L ,47797631L ,47213965L ,494682033L,
                47815137L ,47385873L ,47395893L,49088240L ,49009355L ,48840028L,47815138L ,48840029L);

        List<Long> deletedTpns = Arrays.asList(494682031L, 494682032L, 494682033L, 494682034L, 494682035L);
        List<Long> deletedRecordsList
                = List.of(
                recordList.get(0),
                recordList.get(3),
                recordList.get(8)
        );

        recordList.forEach(tpn -> {
            Map<String, Object> message = Map.of("tripPlanNumber", tpn);
            jsonBodyMap.put("operation", "CREATE");
            jsonBodyMap.put("data", message);
            jsonBodyMap.put("table", "trip_detail1");

            jsonMetadata.put("src", "DBNG");
            jsonMetadata.put("createdTime", new Date().getTime());
            jsonMetadata.put("trackingId", "a6068be6-f212-4ee2-8b90-8db3b8c31942");

            jsonKafkaRecord.put("body", jsonBodyMap);
            jsonKafkaRecord.put("metadata", jsonMetadata);

            try {
                if (!deletedTpns.contains(tpn)) {
                    jsonBodyMap.replace("operation", "UPDATE");
                    String json2 = objectMapper.writeValueAsString(jsonKafkaRecord);
                    kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json2);
                }else{
                    message = Map.of("tripPlanNumber", tpn);
                    jsonBodyMap.replace("operation", "DELETE");
                    jsonBodyMap.replace("data", message);
                    String json3 = objectMapper.writeValueAsString(jsonKafkaRecord);
                    kafkaProducer.sendMessage(topicName, message.get("tripPlanNumber").toString(), json3);
                }

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
    }

    Map<String, Object> getBodyJson(Map<String, Object> message, String tableName){
        Map<String, Object> jsonBodyMap = new HashMap<>();
        jsonBodyMap.put("operation", "INSERT");
        jsonBodyMap.put("data", message);
        jsonBodyMap.put("table", tableName);

        return jsonBodyMap;
    }
    Map<String, Object> getJsonMetadata(){
        Map<String, Object> jsonMetadata = new HashMap<>();
        jsonMetadata.put("src", "DBNG");
        jsonMetadata.put("createdTime", new Date().getTime());
        jsonMetadata.put("trackingId", "a6068be6-f212-4ee2-8b90-8db3b8c31942");

        return jsonMetadata;
    }

}