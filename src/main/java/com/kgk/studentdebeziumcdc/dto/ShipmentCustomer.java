package com.kgk.studentdebeziumcdc.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShipmentCustomer {
    public Long tripPlanNumber;

    public Long orgPoolId;
}
