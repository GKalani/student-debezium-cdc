package com.kgk.studentdebeziumcdc.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShipmentKafkaRecord {
    private String operation;
    private String table;
    private ShipmentData data;
}
