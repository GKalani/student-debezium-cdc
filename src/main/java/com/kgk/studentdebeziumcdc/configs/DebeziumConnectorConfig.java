package com.kgk.studentdebeziumcdc.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DebeziumConnectorConfig {
    /**
     * Database details.
     */
    @Value("${datasource.host}")
    protected String datasourceHost;

    @Value("${database.name}")
    protected String databaseName;

    @Value("${datasource.port}")
    protected String datasourcePort;

    @Value("${datasource.username}")
    protected String datasourceUsername;

    @Value("${datasource.password}")
    protected String datasourcePassword;

}
