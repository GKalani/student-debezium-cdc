package com.kgk.studentdebeziumcdc.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StudentConnectorConfig extends DebeziumConnectorConfig{
    /**
     * Student database connector.
     *
     * @return Configuration.
     */
    @Bean
    public io.debezium.config.Configuration studentConnector() {
        String STUDENT_TABLE_NAME = "public.students";
        //.with("execute-snapshot", "incremental")
        return io.debezium.config.Configuration.create()
                .with("connector.class", "io.debezium.connector.postgresql.PostgresConnector")
                .with("offset.storage",  "org.apache.kafka.connect.storage.FileOffsetBackingStore")
                .with("offset.storage.file.filename", "student-offset.dat")
                .with("offset.flush.interval.ms", 60000)
                .with("plugin.name","pgoutput")
                .with("snapshot.mode", "never")
                .with("name", "student-postgres-connector")
                .with("database.server.name", datasourceHost+"-"+databaseName)
                .with("database.hostname", datasourceHost)
                .with("database.port", datasourcePort)
                .with("database.user", datasourceUsername)
                .with("database.password", datasourcePassword)
                .with("database.dbname", databaseName)
                .with("topic.prefix", "hello")
                .with("table.include.list", STUDENT_TABLE_NAME).build();
    }
}
