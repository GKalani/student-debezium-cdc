package com.kgk.studentdebeziumcdc.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kgk.studentdebeziumcdc.kafka.KafkaProducer;
import com.kgk.studentdebeziumcdc.utils.Operation;
import io.debezium.config.Configuration;
import io.debezium.embedded.Connect;
import io.debezium.embedded.EmbeddedEngine;
import io.debezium.engine.DebeziumEngine;
import io.debezium.engine.RecordChangeEvent;
import io.debezium.engine.format.ChangeEventFormat;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.kafka.connect.data.Field;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.source.SourceRecord;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static io.debezium.data.Envelope.FieldName.*;
import static java.util.stream.Collectors.toMap;

@Slf4j
@Component
public class StudentCDCListener {
    /**
     * Single thread pool which will run the Debezium engine asynchronously.
     */
    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    /**
     * The Debezium engine which needs to be loaded with the configurations, Started and Stopped - for the
     * CDC to work.
     */
    private final DebeziumEngine engine;

    private final KafkaProducer kafkaProducer;

    /**
     * Constructor which loads the configurations and sets a callback method 'handleEvent', which is invoked when
     * a DataBase transactional operation is performed.
     *
     * @param studentConnector
     */
    private StudentCDCListener(Configuration studentConnector, KafkaProducer kafkaProducer) {
        //this.engine = executeEmbeddedEngine(studentConnector);
        this.kafkaProducer = kafkaProducer;
        this.engine = executeDebeziumEngine(studentConnector);
    }

    /**
     * The method is called after the Debezium engine is initialized and started asynchronously using the Executor.
     */
    @PostConstruct
    private void start() {
        this.executor.execute(engine);
    }

    /**
     * This method is called when the container is being destroyed. This stops the debezium, merging the Executor.
     */
    @PreDestroy
    private void stop() throws IOException {
        if (this.engine != null) {
            //this.engine.stop();
            this.engine.close();
        }
    }

    /**
     * Change Detect Capturing using DebeziumEngine
     * @param studentConnector
     * @return
     */
    private DebeziumEngine<RecordChangeEvent<SourceRecord>> executeDebeziumEngine(Configuration studentConnector){
        final Properties connectorProps = studentConnector.asProperties();

        return DebeziumEngine.create(ChangeEventFormat.of(Connect.class))
                .using(connectorProps)
                .notifying(this::handleEventForDebeziumEngine)
                .build();
    }

    /**
     * This method is invoked when a transactional action is performed on any of the tables that were configured.
     *
     * @param sourceRecord
     */
    private void handleEventForDebeziumEngine(RecordChangeEvent<SourceRecord> sourceRecord) {
        log.info("Student Connector");
        Struct sourceRecordValue = (Struct) sourceRecord.record().value();

        if(sourceRecordValue != null) {
            Operation operation = Operation.forCode((String) sourceRecordValue.get(OPERATION));

            //Only if this is a transactional operation.
            // if(operation != Operation.READ) {

            Map<String, Object> message;
            String record = AFTER; //For Update & Insert operations.

            if (operation == Operation.DELETE) {
                record = BEFORE; //For Delete operations.
            }

            //Build a map with all row data received.
            Struct struct = (Struct) sourceRecordValue.get(record);
            message = struct.schema().fields().stream()
                    .map(Field::name)
                    .filter(fieldName -> struct.get(fieldName) != null)
                    .map(fieldName -> Pair.of(fieldName, struct.get(fieldName)))
                    .collect(toMap(Pair::getKey, Pair::getValue));

            log.info("Data Changed: {} with Operation: {}", message, operation.name());

            ObjectMapper objectMapper = new ObjectMapper();
            /*
            * Create Map for Json Obj
            * */
            Map<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("operation", operation.name());
            jsonMap.put("data", message);
            jsonMap.put("table", "students");

            try {
                String json = objectMapper.writeValueAsString(jsonMap);
                kafkaProducer.sendMessageWithKey(message.get("id").toString(), json);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Change Detect Capturing using Debezium Embedded Engine
     * @param studentConnector
     * @return
     */

    private EmbeddedEngine executeEmbeddedEngine(Configuration studentConnector){
        return EmbeddedEngine
                .create()
                .using(studentConnector)
                .notifying(this::handleEventForEmbeddedEngine)
                .build();
    }
    private void handleEventForEmbeddedEngine(SourceRecord sourceRecord) {
        log.info("Student Connector");
        Struct sourceRecordValue = (Struct) sourceRecord.value();

        if(sourceRecordValue != null) {
            Operation operation = Operation.forCode((String) sourceRecordValue.get(OPERATION));

            //Only if this is a transactional operation.
            // if(operation != Operation.READ) {

            Map<String, Object> message;
            String record = AFTER; //For Update & Insert operations.

            if (operation == Operation.DELETE) {
                record = BEFORE; //For Delete operations.
            }

            //Build a map with all row data received.
            Struct struct = (Struct) sourceRecordValue.get(record);
            message = struct.schema().fields().stream()
                    .map(Field::name)
                    .filter(fieldName -> struct.get(fieldName) != null)
                    .map(fieldName -> Pair.of(fieldName, struct.get(fieldName)))
                    .collect(toMap(Pair::getKey, Pair::getValue));

            //Call the service to handle the data change.
            //this.studentService.maintainReadModel(message, operation);
            log.info("Data Changed: {} with Operation: {}", message, operation.name());
            //}
        }
    }


}
