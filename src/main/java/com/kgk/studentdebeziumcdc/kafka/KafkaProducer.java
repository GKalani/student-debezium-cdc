package com.kgk.studentdebeziumcdc.kafka;

import com.kgk.studentdebeziumcdc.constants.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class KafkaProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(Map<String, Object> message){
        LOGGER.info(String.format("Message sent -> %s ", message));
        //kafkaTemplate.send(AppConstants.TOPIC_NAME, "message", message);
    }

    public void sendMessageWithKey(String key, String message){
        try {
            kafkaTemplate.send(AppConstants.TOPIC_NAME, key, message);
            LOGGER.info(String.format("Message sent  -> %s ", message));
        }catch (Exception ex){
            LOGGER.error(String.format("Message does not send because of the exception :  ", ex.getMessage()));
        }
    }

    public void sendMessage(String topic,String key, String message) {
        try {
            kafkaTemplate.send(topic, key, message);
            LOGGER.info(String.format("Message %s sent to  -> %s topic ", message, topic));
        }catch (Exception ex){
            LOGGER.error(String.format("Message does not send because of the exception :  ", ex.getMessage()));
        }
    }
}
