package com.kgk.studentdebeziumcdc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentDebeziumCdcApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentDebeziumCdcApplication.class, args);
	}

}
